﻿namespace App1
{
    using System;
    using Windows.UI.Popups;
    using Windows.UI.Xaml.Controls;

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            editTextBox.Text = @"This is some sample text to show off the problem described.
You can change it to anything you want.
However the issue will only be seen if there are line breaks & you attempt a position after a line break.
For this text good positions to experiment with is:
50 - It is on the first line, so both TextBox & String functions should match
70 - It is on the second line, so it should be out by one
110 - It is on the third line, so it shoukd be out by two";
        }

        private void TextBoxFunctionsButtonClick(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            editTextBox.Focus(Windows.UI.Xaml.FocusState.Keyboard);
            editTextBox.Select(Convert.ToInt32(Position.Text), 1);
        }

        private async void StringFunctionsButtonClick(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var character = editTextBox.Text.Substring(Convert.ToInt32(Position.Text), 1);
            var dialog = new MessageDialog("The character at position " + Position.Text + " is :" + character);
            await dialog.ShowAsync();
        }

        private async void AdjustedStringFunctionButtonClick(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var editText = editTextBox.Text;
            var position = Convert.ToInt32(Position.Text);
            var difference = 0;
            var scanPosition = 0;
            while ((scanPosition = editText.IndexOf("\r\n", scanPosition + 1)) > -1)
            {
                if (scanPosition > position)
                {
                    break;
                }

                difference++;
            }

            var character = editText.Substring(position + difference, 1);

            var dialog = new MessageDialog("The character at position " + Position.Text + " is :" + character);
            await dialog.ShowAsync();
        }
    }
}
